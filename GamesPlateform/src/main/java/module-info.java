module fr.GamesPlatform.newPacMan {
    requires javafx.controls;
    requires javafx.fxml;
	requires javafx.graphics;
	requires javafx.base;
	requires javafx.media;
	requires java.sql;

	opens fr.GamesPlateform to javafx.fxml;
    exports fr.GamesPlateform;

}