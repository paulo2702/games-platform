package fr.GamesPlateform;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
//Class for control the page admin fxml
public class AdminControlleur implements Initializable {
	@FXML
	private Button retourne;
	@FXML
	private TextField addUsername;
	@FXML
	private Button add;
	@FXML
	private TextField addPassword;
	@FXML
	private TextField addEmail;
	@FXML
	private TextField suprUsername;
	@FXML
	private Button supr;
	@FXML
	private Button delGame;
	@FXML
	private TextField addNameGame;
	@FXML
	private TextField suprNameGame;
	@FXML
	private Label info;
	@FXML
	private Label info2;
	@FXML
	private Label info3;
	@FXML
	private Label info4;
	@FXML
	private TextField addDescGame;
	@FXML
	private TextField addUrlVideo;
	@FXML
	private Button deco;
	@FXML
	private TextField addButton;
	@FXML
	private TextField addRootGame;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}
	// method for go to the page appli when we press the return button
	@FXML
	void goToAppli(ActionEvent event) throws IOException {
		retourne.getScene().getWindow().hide();
		Stage appli = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("Appli.fxml"));
		Scene scene = new Scene(root);
		appli.setScene(scene);
		appli.show();
		appli.setResizable(false);
	}
	// method for add new user -- enter username, password, email
	@FXML
	void addNewUser() {
		String userName = addUsername.getText();
		String password = addPassword.getText();
		String email = addEmail.getText();

		var db = DBConnection.getInstance();
		int status = 0;
		ResultSet res = db.query("SELECT email FROM joueur where email='" + email + "';");
		try {
			if (res.next()) {
				status++;
				info.setText("Email not valid");
			}
			ResultSet res1 = db.query("SELECT username FROM joueur where username='" + userName + "';");

			if (res1.next()) {
				status++;
				info.setText("Name already used ");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (status == 0) {
			DBConnection.getInstance().insertUser(userName, email, password);
			info.setText("User registred");
			addUsername.clear();
			addPassword.clear();
			addEmail.clear();
			System.out.println("User registred");
		}
	}
	// method for delete one user -- enter his name for delete
	@FXML
	void deleteUser() throws SQLException {
		var suprUser = suprUsername.getText();
		DBConnection.getInstance().deleteUser(suprUser);
		info2.setText("User delete");
		suprUsername.clear();
		System.out.println("User delete");
	}
	// method for add the information in database of one game -- enter name of game for delete
	@FXML
	void addGame() throws SQLException {
		String name = addNameGame.getText();
		String description = addDescGame.getText();
		String urlVideo = addUrlVideo.getText();
		String button = addButton.getText();
		String rootGame = addRootGame.getText();

		DBConnection.getInstance().addGame(name, description, urlVideo, button, rootGame);
		info3.setText("Game registred");
		System.out.println("Game registred");
	}
	// method for delete the information in database of one game -- enter name of game for delete
	@FXML
	void deleteGame() throws SQLException {
		String deleteGame = suprNameGame.getText();
		DBConnection.getInstance().deleteGame(deleteGame);
		info4.setText("Game delete");
		suprNameGame.clear();
		System.out.println("Game delete");
	}
	// method for deconnexion when we press button "deconnexion"
	@FXML
	public void deconnexion() throws IOException {
		deco.getScene().getWindow().hide();
		Stage appli = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
		Scene scene = new Scene(root);
		appli.setScene(scene);
		appli.show();
		appli.setResizable(false);
	}
}