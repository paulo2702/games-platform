package fr.GamesPlateform;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
// class for control signup fxml
public class SignupController implements Initializable {
	@FXML
	private Label errorusername;
	@FXML
	private Label errorpassword;
	@FXML
	private Label erroremail;
	@FXML
	private TextField username;
	@FXML
	private PasswordField password;
	@FXML
	private TextField age;
	@FXML
	private Button signup;
	@FXML
	private Button login;
	@FXML
	private TextField email;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		username.setStyle("-fx-text-inner-color: #a0a2ab;");
		password.setStyle("-fx-text-inner-color: #a0a2ab;");
		email.setStyle("-fx-text-inner-color: #a0a2ab;");
	}

	// method for go to login fxml when we press signup button
	@FXML
	public void signUP(ActionEvent ae1) throws IOException {
		System.out.print("SignUP Successfull");
		signup.getScene().getWindow().hide();
		Stage Appli = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
		Scene scene = new Scene(root);
		Appli.setScene(scene);
		Appli.show();
		// Appli.setResizable(false);
	}

	// method for go to login fxml when we press login button
	@FXML
	public void loginAction() throws IOException {
		signup.getScene().getWindow().hide();
		Stage login = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
		Scene scene = new Scene(root);
		login.setScene(scene);
		login.show();
		login.setResizable(false);

	}

	// method for register one user -- enter username, password and email -- error
	// if username and email Already exists
	public void signup() {
		errorusername.setText(null);
		erroremail.setText(null);
		errorpassword.setText(null);
		String usernameS = username.getText();
		String emailS = email.getText();
		String passwordS = password.getText();
		var db = DBConnection.getInstance();
		int status = 0;
		ResultSet res = db.query("SELECT email FROM joueur where email='" + emailS + "';");
		try {
			if (res.next()) {
				status++;
				erroremail.setText("Email not valid");
			}
			ResultSet res1 = db.query("SELECT username FROM joueur where username='" + usernameS + "';");

			if (res1.next()) {
				status++;
				errorusername.setText("Name already used ");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (status == 0) {
			DBConnection.getInstance().insertUser(usernameS, emailS, passwordS);
			try {
				signup.getScene().getWindow().hide();
				Stage login = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
				Scene scene = new Scene(root);
				login.setScene(scene);
				login.show();
				login.setResizable(false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("user registred");
		}
	}
}
