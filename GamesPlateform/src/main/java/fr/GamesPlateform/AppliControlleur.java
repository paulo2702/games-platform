package fr.GamesPlateform;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.media.*;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
// class for control appli fxml
public class AppliControlleur implements Initializable {

	@FXML
	private Button admin;
	@FXML
	private Button mario;
	@FXML
	private AnchorPane ap;
	@FXML
	private Button sheepteroids;
	@FXML
	private Button home;
	@FXML
	private Button classement;
	@FXML
	private BorderPane bp;
	@FXML
	private Button play;
	@FXML
	private MediaView mediaview;
	@FXML
	private MediaPlayer mediaPlayer;
	@FXML
	private Media media;
	@FXML
	private Button deco;
	@FXML
	private Button perso;
	@FXML
	private Button flappyBird;
	@FXML
	private Button spaceGame;
	

	public static String game;
	public static Integer id;
	// method for load borderpane of classement
	@FXML
	void classement(MouseEvent event) throws IOException, SQLException {
		loadPage("classement");
	}
	// method for load borderpane of game -- sheepteroids
	@FXML
	void pacman() throws IOException, SQLException {
		game = "sheepteroids";
		loadPage("presentation");
	}
	// method for load borderpane of game -- mario
	@FXML
	void memory() throws IOException, SQLException {
		game = "mario";
		loadPage("presentation");
		
	}
	// method for load borderpane of game -- flapybird	
	@FXML
	void flappy() throws IOException, SQLException {
	game = "flappybird";
	loadPage("presentation");
	}
	// method for load borderpane of game -- spacegame
	@FXML
	void space() throws IOException, SQLException {
		game = "spacegame";
		loadPage("presentation");
	}
	// method for load borderpane of home page
	@FXML
	void home() {
		bp.setTop(ap);
	}
	// method for load fxml page
	@FXML
	private void loadPage(String page) throws IOException, SQLException {
		Parent root = null;
		root = FXMLLoader.load(getClass().getResource(page + ".fxml"));
		bp.setTop(root);
	}
	// method for control if user is an admin or not -- if yes he can press to admin button and go to
	@FXML
	public void adminAction() throws IOException, SQLException {
		var db = DBConnection.getInstance();
		ResultSet res = db.query("SELECT admin FROM joueur WHERE id = " + LoginControlleur.id + ";");
		// id = res.getInt("id");
		// System.out.println(id);
		res.next();
		if (res.getInt("admin") == 1) {
			admin.getScene().getWindow().hide();
			Stage administrator = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("admin.fxml"));
			Scene scene = new Scene(root);
			administrator.setScene(scene);
			administrator.show();
			administrator.setResizable(false);
		} else {
			admin.getScene().getWindow().hide();
			Stage login = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
			Scene scene = new Scene(root);
			login.setScene(scene);
			login.show();
			login.setResizable(false);
		}
	}
	// method for set visible or not the button : admin perso and deconnexion -- if user admin => admin and deconnexion visible
	// if user not admin => perso and deconnexion visible
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		var db = DBConnection.getInstance();
		ResultSet res = db.query("SELECT admin FROM joueur WHERE id = " + LoginControlleur.id + ";");
		try {
			res.next();
			if (res.getInt("admin") != 1) {
				admin.setVisible(false);
				perso.setVisible(true);
				deco.setVisible(true);
			} else {
				admin.setVisible(true);
				perso.setVisible(false);
				deco.setVisible(true);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// method for go to login page when we press on button deconnexion
	@FXML
	public void deconnexion() throws IOException {
		deco.getScene().getWindow().hide();
		Stage appli = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
		Scene scene = new Scene(root);
		appli.setScene(scene);
		appli.show();
		appli.setResizable(false);
	}
	// method for go to perso page when we press on button perso
	@FXML
	public void perso() throws IOException {
		perso.getScene().getWindow().hide();
		Stage appli = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("perso.fxml"));
		Scene scene = new Scene(root);
		appli.setScene(scene);
		appli.show();
		appli.setResizable(false);
	}
}
