package fr.GamesPlateform;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
// class for control perso fxml
public class Perso implements Initializable {

	@FXML
	private Button retourne;
	@FXML
	private TextField modifyUsername;
	@FXML
	private Button modify;
	@FXML
	private TextField modifyPassword;
	@FXML
	private TextField modifyEmail;
	@FXML
	private TextField passForDel;
	@FXML
	private Button delAccount;
	@FXML
	private Label info;
	@FXML
	private Label info2;
	@FXML
	private Button deco;
	@FXML
	private Button bmodifyEmail;
	@FXML
	private Button bmodifyPassword;
	@FXML
	private Button bmodifyUsername;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}
	// method for modify his username
	public void modifUsername() throws SQLException {
		String modifUsername = modifyUsername.getText();
		DBConnection.getInstance().updateUsername(modifUsername);
		info.setText("Username Registred");
		System.out.println("Username Registred");
		modifyUsername.clear();
	}
	// method for modify his password
	public void modifPassword() throws SQLException {
		String modifPassword = modifyPassword.getText();
		DBConnection.getInstance().updatePassword(modifPassword);
		info.setText("Password Registred");
		System.out.println("Password Registred");
		modifyPassword.clear();
	}
	// method for modify his email
	public void modifEmail() throws SQLException {
		String modifEmail = modifyEmail.getText();
		DBConnection.getInstance().updateEmail(modifEmail);
		info.setText("Email Registred");
		System.out.println("Email Registred");
		modifyEmail.clear();
	}
	// method for go to appli page when we press return button
	public void goReturn() throws IOException {
		retourne.getScene().getWindow().hide();
		Stage appli = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("Appli.fxml"));
		Scene scene = new Scene(root);
		appli.setScene(scene);
		appli.show();
		appli.setResizable(false);
	}
	// method for go to login page when we press deconnexion button
	@FXML
	public void deconnexion() throws IOException {
		deco.getScene().getWindow().hide();
		Stage appli = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
		Scene scene = new Scene(root);
		appli.setScene(scene);
		appli.show();
		appli.setResizable(false);
	}
	// method for delete his account -- redirect to login page
	@FXML
	void deleteMe() throws SQLException, IOException {
		var passDel = passForDel.getText();
		DBConnection.getInstance().deleteMe(passDel);
		info2.setText("User delete");
		passForDel.clear();
		System.out.println("User delete");
		passForDel.getScene().getWindow().hide();
		Stage appli = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
		Scene scene = new Scene(root);
		appli.setScene(scene);
		appli.show();
		appli.setResizable(false);
	}

}