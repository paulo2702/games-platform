package fr.GamesPlateform;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
// class for control presentation fxml
public class PresentationController implements Initializable {

	@FXML
	private Label titre;
	@FXML
	private Label description;
	@FXML
	private Button play;
	@FXML
	private BorderPane bp1;
	@FXML
	private AnchorPane ap;
	@FXML
	private MediaView mediaview;
	@FXML
	private MediaPlayer mediaPlayer;
	@FXML
	private Media media;
	// method to take information from games in the database and transcribe it into a boderpane
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		ResultSet rs = DBConnection.getInstance()
				.query("SELECT * FROM jeu where nom = '" + AppliControlleur.game + "';");
		try {
			rs.next();
			String nomS = rs.getString(2);
			String desc = rs.getString(3);
			String playS = rs.getString(5);
			description.setText(desc);
			titre.setText(nomS);
			play.setText(playS);
			String path = rs.getString(4);
			Media media = new Media(new File(path).toURI().toString());
			mediaPlayer = new MediaPlayer(media);
			mediaview.setMediaPlayer(mediaPlayer);
			mediaPlayer.setAutoPlay(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	// method for set the game pacman
	// NOT USED FOR THE MOMENT
	public void setPacman() throws IOException, SQLException {
		ResultSet rs = DBConnection.getInstance()
				.query("SELECT * FROM jeu where nom = '" + AppliControlleur.game + "';");
		rs.next();
		String chemin = rs.getString(6);
		Runtime runtime = Runtime.getRuntime();
		runtime.exec(chemin);
		mediaPlayer.pause();
	}

}
