package fr.GamesPlateform;

import java.sql.*;

//Class for connect the database
public class DBConnection {
	static private DBConnection instance;
	private PreparedStatement insertStatement;
	private PreparedStatement deleteStatement;
	private PreparedStatement addGameStatement;
	private PreparedStatement deleteGameStatement;
	private PreparedStatement updateStatement;
	Connection snx;

	public DBConnection() {

		String url = "jdbc:mysql://localhost/projet_java?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

		String user = "root", pswd = "Acdckiss20juin2009";

		try {
			cnx = DriverManager.getConnection(url, user, pswd);
			insertStatement = cnx.prepareStatement("INSERT INTO joueur (username,email,password) VALUE(?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private Connection cnx = null;

	public static DBConnection getInstance() {

		if (instance != null) {
			return instance;
		}
		instance = new DBConnection();
		return instance;
	}

	public ResultSet query(String queryString) {
		Statement stm;
		ResultSet res = null;
		try {
			stm = cnx.createStatement();
			res = stm.executeQuery(queryString);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	// method for insert user in database -- you must enter the name, password and email -- 
	public boolean insertUser(String username, String email, String password) {
		try {
			insertStatement.setString(1, username);
			insertStatement.setString(2, email);
			insertStatement.setString(3, password);
			int inserted = insertStatement.executeUpdate();
			ResultSet res = insertStatement.getGeneratedKeys();
			if (res.next() && inserted > 0) {
				var lastId = res.getInt(1);
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	// method for delete user in database -- enter username for delete all information of user
	public void deleteUser(String userName) {
		try {
			deleteStatement = cnx.prepareStatement("DELETE FROM joueur WHERE username= ?");
			deleteStatement.setString(1, userName);
			deleteStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// method for add the name, description, video, button, and root of game, for add a new game in database
	public boolean addGame(String nom, String description, String video, String bouton, String cheminjeu)
			throws SQLException {
		addGameStatement = cnx.prepareStatement(
				"INSERT INTO jeu (nom,description,video,bouton,cheminjeu) VALUE(?,?,?,?,?)",
				Statement.RETURN_GENERATED_KEYS);
		addGameStatement.setString(1, nom);
		addGameStatement.setString(2, description);
		addGameStatement.setString(3, video);
		addGameStatement.setString(4, bouton);
		addGameStatement.setString(5, cheminjeu);
		int insertedGame = addGameStatement.executeUpdate();
		ResultSet resGame = addGameStatement.getGeneratedKeys();
		if (resGame.next() && insertedGame > 0) {
			var lastId = resGame.getInt(1);
			return true;
		}
		return false;
	}
	// method for delete information of one game in database
	public void deleteGame(String nom) throws SQLException {
		deleteGameStatement = cnx.prepareStatement("DELETE FROM jeu WHERE nom= ?");
		deleteGameStatement.setString(1, nom);
		deleteGameStatement.execute();
	}
	
	// method for modify username of user
	public void updateUsername(String username) throws SQLException {
		updateStatement = cnx.prepareStatement("UPDATE joueur SET username=? WHERE id=?");
		updateStatement.setString(1,username);
		updateStatement.setInt(2, LoginControlleur.id);
		updateStatement.executeUpdate();
	}
	// method for modify password of user
	public void updatePassword(String password) throws SQLException {
		updateStatement = cnx.prepareStatement("UPDATE joueur SET password=? WHERE id=?");
		updateStatement.setString(1,password);
		updateStatement.setInt(2, LoginControlleur.id);
		updateStatement.executeUpdate();
	}
	// method for modify Email of user
	public void updateEmail(String email) throws SQLException {
		updateStatement = cnx.prepareStatement("UPDATE joueur SET email=? WHERE id=?");
		updateStatement.setString(1,email);
		updateStatement.setInt(2, LoginControlleur.id);
		updateStatement.executeUpdate();
	}
	// method for delete his account
	public void deleteMe(String password) {
		try {
			deleteStatement = cnx.prepareStatement("DELETE FROM joueur WHERE password= ?");
			deleteStatement.setString(1, password);
			//deleteStatement.setInt(2, LoginControlleur.id);
			deleteStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}