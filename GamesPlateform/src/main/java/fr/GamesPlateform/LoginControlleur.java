package fr.GamesPlateform;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

// class for control login fxml
public class LoginControlleur implements Initializable {
	@FXML
	private TextField username;
	@FXML
	private PasswordField password;
	@FXML
	private Button signup;
	@FXML
	private Button login;
	@FXML
	private CheckBox remember;
	@FXML
	private Button forgotpassword;
	@FXML
	private Label error;

	public static Integer id;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		username.setStyle("-fx-text-inner-color: #a0a2ab;");
		password.setStyle("-fx-text-inner-color: #a0a2ab;");
	}

	// method for go to signup page when we press button signup
	@FXML
	public void signUP(ActionEvent e1) {
		login.getScene().getWindow().hide();
		try {
			Stage signup = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("SignUP.fxml"));
			Scene scene = new Scene(root);
			signup.setScene(scene);
			signup.show();
			signup.setResizable(false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// method to connect, it checks if the name and password are entered in the
	// database and match -
	// if this is the case redirect to page appli otherwise error message
	public void loginAction() throws SQLException, IOException {
		error.setText(null);
		String usernameS = username.getText();
		String passwordS = password.getText();
		var db = DBConnection.getInstance();
		String request = "SELECT id, username, password, admin FROM joueur where username ='" + usernameS + "'";
		ResultSet res = db.query(request);

		if (!res.next()) {
			error.setText("Unknown name");
			return;
		}
		String verif = res.getString("Password");
		if (verif.equals(passwordS)) {
			id = res.getInt("id");
			if (res.getInt("admin") == 1) {
				login.getScene().getWindow().hide();
				Stage appli = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("admin.fxml"));
				Scene scene1 = new Scene(root);
				appli.setScene(scene1);
				appli.show();
				appli.setFullScreen(false);
				appli.setResizable(true);
				System.out.print("Login Succesfully");
			} else {
				login.getScene().getWindow().hide();
				Stage appli = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource("Appli.fxml"));
				Scene scene1 = new Scene(root);
				appli.setScene(scene1);
				appli.show();
				appli.setFullScreen(false);
				appli.setResizable(true);
				System.out.print("Login Succesfully");
			}
		} else {
			error.setText("incorrect password");
		}
	}
}
