CREATE DATABASE  IF NOT EXISTS `projet_java` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `projet_java`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: projet_java
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  `id_jeu` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_jeu_idx` (`id_jeu`),
  CONSTRAINT `id_jeu` FOREIGN KEY (`id_jeu`) REFERENCES `jeu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jeu`
--

DROP TABLE IF EXISTS `jeu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jeu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `video` varchar(500) DEFAULT NULL,
  `bouton` varchar(45) NOT NULL,
  `cheminjeu` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jeu`
--

LOCK TABLES `jeu` WRITE;
/*!40000 ALTER TABLE `jeu` DISABLE KEYS */;
INSERT INTO `jeu` VALUES (1,'Sheepteroids','Sheepteroids est un shoot \'em up multidirectionnel au concept simple. Le joueur contrôle un vaisseau spatial, représenté en vue de dessus et confronté à des champs de moutons . Le but est de survivre le plus longtemps possible en détruisant les moutons.','src/main/resources/fr/video/astdelu1.mp4','Play','src/main/resources/fr/video/sheep.exe'),(2,'Mario','L\'action se déroule dans un univers fictif nommé le Royaume Champignon où habitent la princesse Toadstool, Peach au Japon, et ses serviteurs, les Toads. Un jour, une horde de Koopas maléfiques envahit le Royaume Champignon et transforme tous ses habitants en briques, engendrant la chute du royaume. Seule la princesse Toadstool peut inverser le sort et restaurer la paix dans le royaume, mais malheureusement, elle se fait kidnapper par Bowser, le roi des Koopas. Mario, le héros de l\'histoire, est mis au courant de la situation catastrophique du royaume et de ses habitants, et décide de partir à l\'aventure pour libérer la princesse Toadstool des griffes de Bowser..','src/main/resources/fr/video/Super Mario Bros. (Japan, USA).mp4','Play','src/main/resources/fr/video/Mario.exe'),(3,'FlappyBird','Flappy Bird est un jeu vidéo d\'obstacles développé au Viêt Nam à Hanoï par Nguyễn Hà Đông.Le gameplay repose sur l\'agilité du joueur, qui doit faire avancer un oiseau dans un environnement à défilement horizontal en tapotant sur l\'écran tactile, tout en évitant des tuyaux présents en haut et en bas de l\'écran. Les règles de jeu sont très simples : lorsque l\'oiseau touche un tuyau ou heurte le sol, la partie est terminée. Le joueur reçoit un point pour chaque tuyau que l\'oiseau évite. Le jeu contient des graphismes équivalents à Super Mario World','src/main/resources/fr/video/FlappyBird.mp4','Play','src/main/resources/fr/video/Flappy_Bird.exe'),(4,'SpaceGame','SpaceGame est un jeu vidéo ou le joueur dirige un vaisseau qui est assailli par des météorites.Le but étant d’éviter toutes ces météorites.','src/main/resources/fr/video/galaxian.mp4','Play','src/main/resources/fr/video/SpaceGame.exe');
/*!40000 ALTER TABLE `jeu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joue`
--

DROP TABLE IF EXISTS `joue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `joue` (
  `id` int(11) NOT NULL,
  `id_joueur` int(11) NOT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`id_joueur`),
  KEY `id_joueur_idx` (`id_joueur`),
  CONSTRAINT `id` FOREIGN KEY (`id`) REFERENCES `jeu` (`id`),
  CONSTRAINT `id_joueur` FOREIGN KEY (`id_joueur`) REFERENCES `joueur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joue`
--

LOCK TABLES `joue` WRITE;
/*!40000 ALTER TABLE `joue` DISABLE KEYS */;
INSERT INTO `joue` VALUES (1,1,NULL);
/*!40000 ALTER TABLE `joue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `joueur`
--

DROP TABLE IF EXISTS `joueur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `joueur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(15) NOT NULL,
  `admin` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `joueur`
--

LOCK TABLES `joueur` WRITE;
/*!40000 ALTER TABLE `joueur` DISABLE KEYS */;
INSERT INTO `joueur` VALUES (1,'a','','1',1),(3,'e','e','e',0),(5,'paulo2','ssfef@hotmail.fr','1',0),(7,'Mick','mick@efdef.fr','chsfqf',0),(11,'z','dtgdrg','q',0),(13,'tests','testss','tests',0),(14,'t','t','t',0),(16,'Michael','coucou','1',0);
/*!40000 ALTER TABLE `joueur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-04 20:59:15
